class User < ActiveRecord::Base
  include Grape::Entity::DSL
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable


  entity :id, :email, :time_zone, :phone, :father_name, :mother_name, :confirmation_token
  attr_accessible :email, :password, :password_confirmation, :remember_me, :phone, :key, :time_zone, :provider, :uid, :user_id, :confirmation_token

  validates :password, :presence => true, :on => :create
  validates :email, :email => true, :if => :email_required?
  validates_confirmation_of :password_confirmation, :on => :create

  def email_required?
    super && provider.blank?
  end

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :title, :body

  def can_use_system?
    true
  end

  def time
    Time.now.in_time_zone(time_zone)
  end

  def welcome
    ""
  end

  def coach?
    self.is_a?(Coach)
  end

  def family?
    self.is_a?(Family)
  end

  def teen?
    self.is_a?(Teen)
  end

  def self.image_style
    { :medium => "300x300>", :thumb => "100x100>", :index => "30x30#", :welcome => "118x134#" }
  end

  def deactivate!
    self.generate_confirmation_token!
    self.update_attribute(:confirmed_at, nil)
  end
end
