class Guideline < ActiveRecord::Base
  attr_accessible :content, :recommendation, :title, :family_id, :active, :teen_id, :default

  validates :title, :presence => true

  default_scope order("created_at ASC")

  scope :default, where(:type => "DefaultGuideline")
  scope :active, where(:active => true)

  alias_attribute :name, :title

  belongs_to :family
  belongs_to :coach
  belongs_to :teen

  def default?
    self.type == "DefaultGuideline" || self.default
  end

  def hint_text
    if active?
      "To deactivate this guideline press here!"
    else
      "To activate this guideline press here!"
    end
  end

end
