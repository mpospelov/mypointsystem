class DefaultGuideline < Guideline
  after_create :make_a_copy_to_all_teens

  def make_a_copy_to_all_teens
    Teen.all.each{|t| t.guidelines << copy}
  end

  def copy
    Guideline.new(content: self.content,
                  recommendation: self.recommendation,
                  title: self.title,
                  default: true,
                  active: false)
  end

end
