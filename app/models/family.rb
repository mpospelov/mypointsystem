class Family < User
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable
  attr_accessible :father_name, :mother_name, :coach_id, :from_api
  attr_accessor :from_api
  has_many :guidelines, dependent: :destroy
  has_many :teens, dependent: :destroy
  has_many :daily_privileges, dependent: :destroy
  has_many :incident_reports, dependent: :destroy

  validates :father_name, :presence => true, :unless => :from_api
  validates :mother_name, :presence => true, :unless => :from_api
  validates :phone, :presence => true, :unless => :from_api
  validates :time_zone, :presence => true, :unless => :from_api

  def email_required?
    super && provider.blank?
  end

  def can_use_system?
    self.father_name.present? &&
    self.mother_name.present? &&
    self.phone.present? &&
    self.time_zone.present?
  end

  def available_guidelines
    (self.guidelines + Guideline.default).sort{|v1,v2| v1.id <=> v2.id}.uniq
  end

  def available_daily_privileges
    (self.daily_privileges << DailyPrivilege.default).order(:position)
  end

  def welcome
    if self.father_name.blank? || self.mother_name.blank?
      "#{self.father_name}#{self.mother_name}"
    else
      "#{self.father_name}-#{self.mother_name}"
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = self.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = self.new(father_name:auth.extra.raw_info.name,
                      provider:auth.provider,
                      uid:auth.uid,
                      password:Devise.friendly_token[0,20],
                      from_api: true
                      )
      user.skip_confirmation!
      user.save!
    end
    user
  end

  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    user = self.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = self.new(father_name: auth.extra.raw_info.name,
                      provider:auth.provider,
                      uid:auth.uid,
                      password:Devise.friendly_token[0,20],
                      from_api: true
                      )
      user.skip_confirmation!
      user.save!
    end
    user
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
end
