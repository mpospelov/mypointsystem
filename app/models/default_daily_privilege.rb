class DefaultDailyPrivilege < DailyPrivilege
  after_create :make_a_copy_to_all_teens

  def make_a_copy_to_all_teens
    Teen.all.each{|t| t.daily_privileges << copy}
  end

  def copy
    DailyPrivilege.new(name: self.name,
                       default: true,
                       active: false,
                       position: self.position)
  end

end

