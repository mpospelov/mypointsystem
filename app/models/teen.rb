class Teen < User
  devise :database_authenticatable, :rememberable, :trackable, :validatable

  attr_accessible :date_of_birth, :name, :avatar, :family_id, :time_zone, :points

  validates :name, :presence => true
  validates :phone, :presence => true
  validates :date_of_birth, :format => {:with => /^(0[1-9]|1[0-2])\/([0-2]\d|3[0-1])\/\d{4}$/,
                                        :message => "Format is wrong. Correct format is MM/DD/YYYY"}

  default_scope order(:created_at)

  belongs_to :family

  has_many :guidelines
  has_many :daily_privileges
  has_many :incident_reports

  has_one :image, as: :file_carrier,
                  dependent: :destroy

  after_create :copy_default_guidelines
  after_create :copy_default_daily_privileges

  before_create :set_up_time_zone

  alias_attribute :avatar, :image

  def avatar=(attr)
    avatar_image = self.create_image
    avatar_image.update_attributes!(data: attr) rescue avatar_image.destroy
  end

  def welcome
    self.name
  end

  def self.beginning_of_day
    #utc_hour = Time.now.utc.hour
    #time_zone_hour_stamp = - utc_hour
    #time_zone_hour_stamp = utc_hour if utc_hour > 11
    #time_zone = ActiveSupport::TimeZone[time_zone_hour_stamp]
    #time_zone = time_zone.to_s.gsub(/\(.*\) /,"")
    #puts "Update Points for #{time_zone}"
    teens = Teen.all.map{|t| t if t.time.hour == 0}.compact.flatten
    teens.each{|t| t.count_points}
    #self.where(:time_zone => time_zone).each{|t| t.count_points}
  end

  def self.image_style
    { :medium => "300x300>", :thumb => "100x100>", :index => "30x30>", :welcome => "118x134#" }
  end

  def count_points
    puts "#{self.name} #{self.points}"
    update_attribute(:points, [50, points + 50].min)
    puts "#{self.name} #{self.points}"
    reports = incident_reports.where{(created_at >= my{(time - 1.day).beginning_of_day}) & (created_at <= my{time.beginning_of_day})}
    new_points = reports.inject(points){|sum, r| sum -= r.points_loss}
    puts "#{new_points}"
    update_attribute(:points, [50, new_points].min)
    puts "#{self.name} #{self.points}"
  end

  private

  def set_up_time_zone
    self.time_zone ||= self.family.time_zone
  end

  def copy_default_guidelines
    self.guidelines << DefaultGuideline.all.map{|g| g.copy}
  end

  def copy_default_daily_privileges
    self.daily_privileges << DefaultDailyPrivilege.all.map{|g| g.copy}
  end

end
