class DailyPrivilege < ActiveRecord::Base
  attr_accessible :family_id, :name, :type, :teens, :active, :position, :teen_id, :default

  validates :name, :presence => true

  scope :default, where(:type => "DefaultDailyPrivilege")
  scope :active, where(:active => true)

  belongs_to :family
  belongs_to :teen

  def hint_text
    if active?
      "To deactivate this daily privilege press here!"
    else
      "To activate this daily privilege press here!"
    end
  end

end
