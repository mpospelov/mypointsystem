class IncidentReport < ActiveRecord::Base

  default_scope order(:created_at).reverse_order

  attr_accessible :severity, :specifics, :teen_id, :type_of_inappropriate_behavior, :family_id, :teen_correction, :teen_appeal, :points_loss, :reviewed, :appeled

  validates :type_of_inappropriate_behavior, :presence => true

  belongs_to :teen
  belongs_to :family


  def review
    if self.teen_correction.present?
      "Self Correct"
    elsif self.teen_appeal.present?
      "Appeal"
    else
      ""
    end
  end

  def severity=(val)
    res = case val
          when "Mild Incident"
            6
          when "Significant Incident"
            18
          when "Major Incident"
            80
          when "Serious Incident"
            200
          end
    self.points_loss = res
  end

  private

end
