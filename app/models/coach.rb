class Coach < User
  attr_accessible :user_ids, :name
  validates :name, :presence => true
  has_many :families


  def user_ids=(attr)
    self.families.clear
    attr.delete_if { |elem| elem.blank? }
    attr.each do |id|
      family = Family.find(id)
      family.coach_id = self.id
      family.save!
    end
  end

  def welcome
    self.name
  end
end
