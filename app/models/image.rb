class Image < UploadedFile
  def url(type)
    self.data.url(type)
  end
end
