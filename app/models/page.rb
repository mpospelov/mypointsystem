class Page < ActiveRecord::Base
  attr_accessible :body, :meta_description, :meta_keywords, :meta_title, :position, :slug, :title, :visible

  scope :visible, where(visible: true).order(:position)

  validates :slug, presence: true
  validates :title, presence: true
  validates :slug, format: { with: /^\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*(\/.*)?[^\/]$/ix,
                             message: "Slug must start with '/' and can't end on '/'. (Example: /lorem/impsum)" }
end
