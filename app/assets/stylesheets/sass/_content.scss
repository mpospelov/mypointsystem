.page-inner {
  .header__bottom {
    border-bottom: 1px solid #e7e6e6;
  }
}

.page-scrolled {
  .content {
    padding: 138px 0 40px 0;
  }
  .header {

  }
  .logo {
    float: left;
    margin-top: 12px;
  }
  nav {
    overflow: hidden;
  }
  .nav_main {
    padding: 0;
    a {
      height: 90px;
      line-height: 90px;
    }
  }
}

.content {
  @extend .wrapper;
  padding: 240px 0 40px 0;
}

.carousel {
  .markers {
    width: 100%;
    left: 33px;
    bottom: 5px;
    position: absolute;
    >ul {
      position: static;
      margin-left: 0;
    }
    li {
      a {
        background: #fff;
        height: 4px;
      }
      &.active a {
        background: #11bce7;
      }
    }
  }
  .slide {
    height: 473px;
    width: 980px;
  }
  .slides {
    height: 473px;
  }
  .description {
    width: 315px;
    right: 0 !important;
    left: auto !important;
    top: 0;
    &.red {
      background: #eb4543 !important;
    }
    &.yellow {
      background: #fcb612 !important;
    }
    &.green {
      background: #93cb3a !important;
    }
    h2 {
      color: #fff;
      font-size: 36px;
      line-height: 1.1;
      padding: 0 0 35px 0;
    }
    p {
      font-size: 20px;
      color: #463332;
    }
    a {
      color: #fff !important;
      font-size: 18px;
    }
  }
}

.learn-more {
  position: absolute;
  bottom: 30px;
  padding: 0 0 0 35px;
  .icon-arrow-right-3 {
    font-size: 24px;
    top: -1px;
    left: 0;
    position: absolute;
  }
}

.nice-link-wrap {
  @include clearfix;
  width: 1015px !important;
}

.nice-link {
  float: none !important;
  position: relative;
  background: #fff;
  font-size: 20px;
  width: 307px;
  height: 130px;
  display: table;
  margin: 0 31px 20px 0;
  overflow: hidden;
  &.double-vertical {
    height: 280px;
  }
  &:hover {
    outline: 0 !important;
    .nice-link__text {
      background: #00ccff;
    }
  }
  @extend .tile;
  * {
    color: #463332 !important;
  }

}

.nice-link__text {
  width: 190px !important;
  height: 130px;
  border: 1px solid #ccc;
  @include box-sizing(border-box);
  border-left: 1px solid #ccc;
  display: table-cell;
  vertical-align: middle;
  text-align: center;
  padding: 0 18px !important;
  span {
    text-align: left;
    display: inline-block;
  }
  &:before {
    content: '';
    position: absolute;
    display: block;
  }
}

.nice-link__ico {
  width: 119px !important;
  img {
    max-width: 110px;
    display: inline-block;
  }
  border-color: #ccc !important;
  @extend .nice-link__text;
  background: #fff !important;
  &:before {
    display: none;
  }
}

.nice-link.right {
  .nice-link__ico {
    border-right: 0;
  }
  .nice-link__text {
    &:before {
      left: 104px;
      top: 20px;
      @include spr(images, right);
    }
  }
  &:hover {
    .nice-link__text {
      border-color: #00ccff;
      &:before {
        background: 0;
        width: 0;
        height: 0;
        border-top: 12.5px solid transparent;
        border-bottom: 12.5px solid transparent;
        border-right: 15px solid #00ccff;
      }
    }
  }
}

.nice-link.left {
  .nice-link__ico {
    border-left: 0;
  }
  .nice-link__text {
    &:before {
      right: 106px;
      top: 20px;
      @include spr(images, left);
    }
  }
  &:hover {
    .nice-link__text {
      border-color: #00ccff;
      &:before {
        background: 0;
        width: 0;
        height: 0;
        border-top: 12.5px solid transparent;
        border-bottom: 12.5px solid transparent;
        border-left: 15px solid #00ccff;
      }
    }
  }
}

.nice-link.top {
  @extend .double-vertical;
  .nice-link__ico {
    border: 1px solid #ccc;
    color: #666;
    font-size: 66px;
    border-top: 0;
    display: block;
    height: 150px !important;
    width: 307px;
    span {
      display: table-cell;
      width: 307px;
      height: 150px !important;
    }
  }
  .nice-link__text {
    display: block;
    height: 130px;
    width: 307px !important;
    span {
      display: table-cell;
      text-align: center;
      vertical-align: middle;
      width: 307px;
      height: 130px;
    }
    &:before {
      left: 140px;
      top: 129px;
      @include spr(images, up);
    }
  }
  &:hover {
    .nice-link__text {
      border-color: #00ccff;
      &:before {
        background: 0;
        width: 0;
        height: 0;
        border-left: 12.5px solid transparent;
        border-right: 12.5px solid transparent;
        border-top: 15px solid #00ccff;
      }
    }
  }
}

.article {
  width: 510px;
  h2 {
    font-size: 52px;
    line-height: 1.2;
    text-transform: uppercase;
    color: #666;
  }
  p {
    font-size: 16px;
    color: #666;
    padding: 0 20px 0 0;
  }
}

.blockquote {
  width: 355px;
  margin: 0 18px 0 0;
  blockquote {
    color: #666;
    font-size: 18px;
    border: none;
    background: none;
    &:before {
      content: '';
      float: left;
      display: block;
      @include spr(images, blockquote);
    }
  }
}

.blockquote__author {
  overflow: hidden;
  margin: 25px 0 0;
}

.blockquote__ava {
  display: table-cell;
  @include border-radius(100%);
  img {
    display: block;
    @include border-radius(100%);
  }
}

.blockquote__author__name {
  color: #666;
  display: table-cell;
  padding: 0 0 0 19px;
  vertical-align: middle;
  font-size: 14px;
  strong {
    font-size: 24px;
    font-weight: 700;
    display: block;
  }
}

aside.control-panel-nav {
  width: 307px;
  margin: 0 48px 0 0;
}

.control-panel-nav {
  @extend .side-nav;
  h3 {
    color: #162028;
    border-bottom: 4px solid #dc3b3a;
    font-size: 24px;
    padding-bottom: 5px;
  }
  a {
    color: #162028 !important;
    font-size: 18px !important;
    padding: 20px 0 20px 25px !important;
    text-decoration: underline;
    &:hover {
      text-decoration: none;
    }
  }
}

.control-panel-nav__sub {
  >a {
    @include transition(all 0.3s);
    &:before {
      @include transition(all 0.3s);
    }

  }
  &.active {
    >a {

      background: #00ccff;
      color: #fff !important;
      text-decoration: none !important;
      position: relative;
      &:before {
        content: '';
        position: absolute;
        left: 26px;
        bottom: -12px;
        display: block;
        border-left: 12px solid transparent;
        border-right: 12px solid transparent;
        border-top: 12px solid #00ccff;
      }
    }
  }
  ul {
    border: 1px solid #ccc;
    border-top: 0;
    display: none;

  }
}