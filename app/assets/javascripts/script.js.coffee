
jQuery ->
  $(".active_switch").click (event) ->
    event.stopPropagation
    $(this).children().first().submit()
  $(".active_switch").hover (->
    li = $(this).parent()
    value = Math.abs(li.attr('data-active') - 1)
    li.attr("data-active", value)
    $(this).parent().find('.hint').show()
  ), ->
    li = $(this).parent()
    value = Math.abs(li.attr('data-active') - 1)
    li.attr("data-active", value)
    $(this).parent().find('.hint').hide()
  $("#sortable").sortable
    axis: "y"
    scroll: true
    update: ->
      $.ajax
        type: "post"
        data: $("#sortable").sortable("serialize")
        dataType: "script"
        complete: (request) ->
          $("#sortable").effect "highlight"

        url: "daily_privileges/sort"
  $( "#sortable" ).disableSelection()

jQuery ->
  $(document).mouseup (e) ->
    container = $("#dialogBox")
    $.Dialog.hide() if container.has(e.target).length is 0



  #Dropable
  #$("li.selected").draggable
  #  appendTo: "ul.listview"
  #  helper: "clone"

  #$("#cart ol").droppable(
  #  activeClass: "ui-state-default"
  #  hoverClass: "ui-state-hover"
  #  accept: ":not(.ui-sortable-helper)"
  #  drop: (event, ui) ->
  #    $(this).find(".placeholder").remove()
  #    $("<li></li>").text(ui.draggable.text()).appendTo this
  #).sortable
  #  items: "li:not(.placeholder)"
  #  sort: ->

  #    # gets added unintentionally by droppable interacting with sortable
  #    # using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
  #    $(this).removeClass "ui-state-default"



