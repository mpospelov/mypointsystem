$(window).bind("page:change", function() {
    scrollHeader();
    $('.control-panel-nav__sub a').click(function () {
        var li = $(this).closest('li');
        li.toggleClass('active');
        li.find('ul').slideToggle(250);
        return false;
    })
    $('#auth-form').click(function () {
        $('.auth-form, .overlay').slideToggle(250)
    })
    $('.overlay').click(function () {
        $(this).fadeOut(200);
        $('.auth-form').slideUp(250)
    })
})
$(document).ready(function () {
    scrollHeader();
    $('.control-panel-nav__sub a').click(function () {
        var li = $(this).closest('li');
        li.toggleClass('active');
        li.find('ul').slideToggle(250);
        return false;
    })
    $('#auth-form').click(function () {
        $('.auth-form, .overlay').slideToggle(250)
    })
    $('.overlay').click(function () {
        $(this).fadeOut(200);
        $('.auth-form').slideUp(250)
    })
})


$(window).scroll(function () {
    scrollHeader()
});


function scrollHeader() {

    $(window).scrollTop() > 0 ?
        $('body').addClass('page-scrolled') :
        $('body').removeClass('page-scrolled')
}
