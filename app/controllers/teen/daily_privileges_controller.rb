class Teen::DailyPrivilegesController < InheritedResources::Base

  def index
    @available_points = current_user.points.to_i
    points_enough = @available_points/10
    points_enough = 0 if points_enough < 0
    @daily_privileges = current_user.daily_privileges.active.order(:position).first(points_enough)
  end

end
