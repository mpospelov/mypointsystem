class Teen::GuidelinesController < InheritedResources::Base

  def index
    @guidelines = current_user.guidelines.active
  end

end
