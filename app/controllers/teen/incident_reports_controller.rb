class Teen::IncidentReportsController < InheritedResources::Base

  def self_correct
    @incident_report =  IncidentReport.find(params[:id])
  end

  def appeal
    @incident_report =  IncidentReport.find(params[:id])
  end

  protected

  def collection
    @incident_reports ||= current_user.incident_reports
  end

end
