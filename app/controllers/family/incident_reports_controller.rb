class Family::IncidentReportsController < InheritedResources::Base

  before_filter :set_variables
  before_filter :check_user

  def history
    @incident_reports = @teen.incident_reports
  end

  def review_self_correct
  end

  def review_appeal
  end

  def create
    @incident_report = IncidentReport.new(params[:incident_report])
    if @incident_report.save
      redirect_to [:history, :family, @teen, :incident_reports], notice: 'Incident Report was successfully created.'
    else
      redirect_to [:history, :family, @teen, :incident_reports], alert: "Fill all fields!"
    end
  end

  def update
    update! do |success, failure|
      success.html {redirect_to request.referer, notice: "#{params[:commit]} was successfully sended!" }
    end
  end

  private

  def set_variables
    @teen = Teen.find(params[:teen_id])
    @incident_report =  params[:id] ? IncidentReport.find(params[:id]) : IncidentReport.new
  end
end
