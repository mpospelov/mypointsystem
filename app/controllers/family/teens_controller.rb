class Family::TeensController < InheritedResources::Base

  before_filter :set_teen
  before_filter :check_user

  def create
    @teen = Teen.new(params[:teen])
    if @teen.save
      redirect_to family_teens_path, notice: 'Teen was successfully created.'
    else
      render action: "edit"
    end
  end

  def index
    @teens = current_user.teens
  end

  def update
    respond_to do |format|
      if @teen.update_attributes(params[:teen])
        format.html { redirect_to request.referer, notice: 'Teen was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def settings
  end

  def daily_priveleges_sort
    @daily_privileges = current_user.daily_privileges << DailyPrivilege.default
    @daily_privileges.each do |daily_privilege|
      daily_privilege.position = params['daily_privilege'].index(daily_privilege.id.to_s) + 1
      daily_privilege.save!
    end
  end

  private

  def set_teen
    params[:id] ? @teen = current_user.teens.find(params[:id]) : @teen = Teen.new
  end

end
