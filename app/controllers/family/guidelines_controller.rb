class Family::GuidelinesController < InheritedResources::Base

  before_filter :set_instance_values
  before_filter :check_user

  def index
    @guidelines = @teen.guidelines
  end

  def edit
  end

  def update
    update! do |success, failure|
      success.html {redirect_to [:family, @teen, :guidelines], notice: "Guideline was successfully updated." }
      success.js { render :layout => "application.js" }
    end
  end

  def create
    if @guideline.update_attributes(params[:guideline])
      render :layout => "application.js"
    else
      flash[:alert] = "Fill all fields!"
      render :layout => "application.js"
    end
  end

  def destroy
    destroy! do |success, failure|
      success.html{redirect_to families_teen_guidelines_path(@teen), notice: "Guideline was successfully destroyed."}
      success.js{ render :layout => "application.js"}
    end
  end

  private

  def set_instance_values
    if current_user.is_a?(Family)
      @teen = current_user.teens.find(params[:teen_id]) if params[:teen_id]
      params[:id] ? @guideline = @teen.guidelines.find(params[:id]) : @guideline = Guideline.new
    else
      @guideline = current_user.guidelines.find(params[:id])
    end
  end

end
