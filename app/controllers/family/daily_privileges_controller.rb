class Family::DailyPrivilegesController < InheritedResources::Base

  before_filter :set_instance_values
  before_filter :check_user
  respond_to :js, :only => [:create, :update, :destroy ]

  def create
    if @daily_privilege.update_attributes(params[:daily_privilege])
      render :layout => "application.js"
    else
      flash[:alert] = "Fill all fields!"
      render :layout => "application.js"
    end
  end

  def index
    @daily_privileges = @teen.daily_privileges.order(:position)
  end

  def update
    update!{ {:layout => "application.js"} }
  end

  def destroy
    destroy! { {:layout => "application.js"} }
  end

  def sort
    @daily_privileges = @teen.daily_privileges
    @daily_privileges.each do |daily_privilege|
      daily_privilege.position = params['daily_privilege'].index(daily_privilege.id.to_s) + 1
      daily_privilege.save!
    end
    render :nothing => true
  end

  private

  def set_instance_values
    @teen = current_user.teens.find(params[:teen_id]) if params[:teen_id]
    params[:id] ? @daily_privilege = @teen.daily_privileges.find(params[:id]) : @daily_privilege = DailyPrivilege.new
  end

end
