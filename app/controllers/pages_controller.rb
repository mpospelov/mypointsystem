class PagesController < ApplicationController

  def show
    @page = Page.visible.find_by_slug("/#{params[:slug]}")
    if @page
      @meta_title = @page.meta_title
      @meta_description = @page.meta_description
      @meta_keywords = @page.meta_keywords
      respond_to do |format|
        format.html
        format.json { render json: @page }
      end
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

end
