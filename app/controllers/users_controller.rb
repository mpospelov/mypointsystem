class UsersController < ApplicationController
  before_filter :check_user
  before_filter :check_possibility_to_use_system, :except => [:account_settings, :update]

  def controll_panel
  end

  def tiles
  end

  def update
    current_user.update_attributes(params[:family])
    redirect_to welcome_path
  end

end
