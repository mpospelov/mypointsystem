class ApplicationController < ActionController::Base

  protect_from_forgery
  before_filter :clear_empty_params
  around_filter :user_time_zone, if: :current_user

  private

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
      if resource.is_a?(Admin)
        admin_dashboard_path
    else
      welcome_path
    end
  end

  #setting time zone
  def user_time_zone(&block)
    Time.use_zone(current_user.time_zone, &block)
  end

  def check_possibility_to_use_system
    unless current_user.can_use_system?
      flash[:notice] = flash[:notice].to_s << "Fill all required fields!"
      return redirect_to account_settings_path
    end
  end

  def check_user
    unless current_user || current_admin_user
      return redirect_to new_user_session_path, :alert => "You must be logged in!"
    end
  end

  # method for clearing params's keys with blank? elements
  def clear_empty_params
    clear_hash(params)
  end

  def clear_hash(hash)
    hash.delete_if do |k, v|
      clear_hash(v) if v.is_a?(Hash)
      v.blank?
    end
  end

end
