ActiveAdmin.register Video do
  actions :all, except: [:destroy, :create, :show, :new]
  menu :label => "Video"
  menu :parent => "3.Static Content"
  config.filters = false
  index do
    column :data_file_name
    column :data_file_size
    column :description
    column :updated_at
    default_actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :data, :as => :file
    end
    f.buttons
  end

end
