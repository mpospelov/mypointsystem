ActiveAdmin.register Coach do
  menu :parent => "1.Users"
  config.filters = false
  index do
    column :email
    column :name
    default_actions
  end
  form :partial => "/admin/shared/form"


  show do |ad|
    attributes_table do
      row :email
      row :name
      table_for coach.families do
        column "Families" do |family|
          "#{family.father_name} - #{family.mother_name}"
        end
      end
    end
    active_admin_comments
  end

end
