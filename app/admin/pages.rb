ActiveAdmin.register Page do
  actions :all, :except => [:show]
  menu :parent => "3.Static Content"
  config.filters = false

  controller do
    def index
      params[:order] = "position_asc" if params[:order].nil?
      super
    end
  end

  index do
    column :title
    column :slug
    column :visible
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug
      f.input :body, as: :ckeditor, input_html: { ckeditor: { language: "en" }}
      f.input :meta_title
      f.input :meta_description
      f.input :meta_keywords

      f.input :visible
    end
    f.actions
  end

  collection_action :sort, :method => :post do
    params[:page].each_with_index do |id, index|
      Page.find(id).update_attribute(:position, index+1)
    end
    render :nothing => true
  end

end
