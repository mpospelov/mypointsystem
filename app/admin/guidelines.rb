ActiveAdmin.register DefaultGuideline do
  menu :parent => "2.Default"
  config.filters = false
  index do
    column :title
    column :recommendation
    column :content
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :recommendation
      f.input :content
    end
    f.buttons
  end

  show do |ad|
    attributes_table do
      row :title
      row :recommendation
      row :content
    end
    active_admin_comments
  end
end
