ActiveAdmin.register DefaultDailyPrivilege do
  menu :parent => "2.Default"
  config.filters = false
  index do
    column :name
    default_actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :position
    end
    f.buttons
  end

  show do |ad|
    attributes_table do
      row :name
    end
    active_admin_comments
  end

end
