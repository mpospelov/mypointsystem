module Private
  class API < Grape::API
    format :json
    desc "Active User"
    post 'activate' do
      user = User.find_by_confirmation_token(params[:confirmation_token])
      error!('401 Unauthorized', 401) unless user
      user.confirm!
      present user
    end
    desc "Deactive User"
    post 'deactivate' do
      user = User.find(params[:id])
      error!('401 Unauthorized', 401) unless user
      user.deactivate!
      present user
    end
    post 'sign_up' do
      user = Family.new(params[:family].merge(from_api: true))
      user.skip_confirmation!
      if user.save
        user.deactivate!
        present user
      else
        user.errors.full_messages.uniq
      end
    end
  end
end
