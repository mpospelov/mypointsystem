module ApplicationHelper
  def clear_hash(hash)
    hash.delete_if do |k, v|
      v.empty?
      if v.is_a?(Hash)
        clear_hash(v)
      end
    end
  end

  def back_link
    @back_link ||= "javascript:history.back()"
    link_to "", "javascript:history.back()", class: "back-button big page-back",
                                             style: "float: left;margin: 20px;"
  end

end
