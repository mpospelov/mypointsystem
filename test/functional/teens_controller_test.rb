require 'test_helper'

class TeensControllerTest < ActionController::TestCase
  setup do
    @teen = teens(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:teens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create teen" do
    assert_difference('Teen.count') do
      post :create, teen: { date_of_birth: @teen.date_of_birth, email: @teen.email, name: @teen.name, password: @teen.password }
    end

    assert_redirected_to teen_path(assigns(:teen))
  end

  test "should show teen" do
    get :show, id: @teen
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @teen
    assert_response :success
  end

  test "should update teen" do
    put :update, id: @teen, teen: { date_of_birth: @teen.date_of_birth, email: @teen.email, name: @teen.name, password: @teen.password }
    assert_redirected_to teen_path(assigns(:teen))
  end

  test "should destroy teen" do
    assert_difference('Teen.count', -1) do
      delete :destroy, id: @teen
    end

    assert_redirected_to teens_path
  end
end
