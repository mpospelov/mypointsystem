Feature: Sign in
  In order to use the "My Point System" service
  As a user
  I want to sign into "My Point System" service

    Background:
      Given I am not logged in

    Scenario: Sign in as a "Family"
      When I sign in as a "Family"
      Then I click "Welcome" link

    Scenario: Sign in as a "Coach"
      When I sign in as a "Coach"
      Then I click "Welcome" link

    Scenario: Sign in as a "Teen"
      When I sign in as a "Teen"
      Then I click "Welcome" link

    Scenario: Sign in with invalid data
      When I sign in with invalid data
      Then I redirected to "new_user_session_path"
