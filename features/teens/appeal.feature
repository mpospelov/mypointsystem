@javascript
Feature: Appeal
  In order to prevent penalty of incident report
  As a teen
  I want to be able to create appeal on incident report

    Scenario: Family create incident report with a valid data
      Given I am logged in as a "family"
      And I have one teen
      And I have one type of inappropriate behavior
      When I create incident report with a valid data
      Given I am logged in as a "teen"
      When I create appeal for incident report
      Given I am logged in as a "family"
      When I review appeal for incident report
      Then I should prevent penalty on next day
