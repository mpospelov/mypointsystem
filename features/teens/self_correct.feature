@javascript
Feature: Self Correct
  In order to decrease penalty of incident report
  As a teen
  I want to be able to create self correct on incident report

    Scenario: Family create incident report with a valid data
      Given I am logged in as a "family"
      And I have one teen
      And I have one type of inappropriate behavior
      When I create incident report with a valid data
      Given I am logged in as a "teen"
      When I create self correct for incident report
      Given I am logged in as a "family"
      When I review self correct for incident report
      Then I should get half of penalty on next day
