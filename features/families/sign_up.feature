Feature: Sign up
  In order to get access to protected sections of the site
  As a family
  I want to be able to sign up

    Background:
      Given I am not logged in

    Scenario: Family signs up with valid data
      When I sign up with valid family data
      Then I redirected to root path

    Scenario: Family signs up with invalid email
      When I sign up with an invalid email
      Then I should see an invalid email message

    Scenario: Family signs up without password
      When I sign up without a password
      Then I should see a missing password message

    Scenario: Family signs up with mismatched password and confirmation
      When I sign up with a mismatched password confirmation
      Then I should see a mismatched password message
