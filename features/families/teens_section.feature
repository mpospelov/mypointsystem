@javascript
Feature: Teen section
  In order to CRUD Teen model
  As a family
  I want to CRUD

    Background:
      Given I am logged in as a family

    Scenario: Family create Teen with valid data
      When I create Teen in Teen's section with valid data
      Then I have a new list item in list of teens

    Scenario: Family create Teen with invalid data
      When I create Teen in Teen's section with invalid data
      Then I should see an error messages

    Scenario: Family update Teen
      When I update Teen in Teen's section
      Then I should see successfull updating message

    Scenario: Create Daily Privileges
      When I create Daily Privilege
      Then I should see active Daily Privilege in teen's list of Daily Privileges

    Scenario: Create default Daily Privileges
      When I create default Daily Privilege
      Then I should see inactive Daily Privilege in teen's list of Daily Privileges

    Scenario: Create Guidelines
      When I create Guidelines
      Then I should see active Guideline in teen's list of Guidelines

    Scenario: Create default Guidelines
      When I create default Guideline
      Then I should see inactive Guideline in teen's list of Guideline
