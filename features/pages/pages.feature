Feature: Pages
  In order to manipulate static content
  As an admin
  I want to be able create and change static pages

    Background:
      Given I am login as an admin

    Scenario: Admin creates page with valid data
      When I create "Page" in admin panel with valid data
      Then I can visit this page by it's slug

    Scenario: Admin creates page with invalid data
      When I create "Page" in admin panel without "title"
      Then I should see an error message "can't be blank"
      When I create "Page" in admin panel without "slug"
      Then I should see an error message "can't be blank"
      When I create "Page" in admin panel with invalid slug
      Then I should see an error message "Slug must start with '/' and can't end on '/'. (Example: /lorem/impsum)"
