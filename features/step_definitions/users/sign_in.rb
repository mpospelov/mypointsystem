When /^I sign in as a "(.*)"$/ do |klass|
  klass = klass.downcase.to_sym
  create_visitor
  create_user(klass)
  visit root_path
  log_in(klass)
end

Then /^I click "(.*?)" link$/ do |link|
  link = "Welcome #{@user.welcome}" if link == "Welcome"
  click_link link
end

When /^I sign in with invalid data$/ do
  create_visitor
  create_user
  @visitor[:family][:password] = ""
  log_in
end

Then /^I redirected to "(.*)"$/ do |path|
  assert current_path == eval(path), ""
end
