require 'debugger'
def create_visitor
  @visitor ||= {
    family:{
      father_name: 'Father',
      mother_name: 'Mother',
      email: 'family@family.com',
      password: 'password',
      password_confirmation: 'password',
      phone: '88000900400',
      time_zone: "Hawaii"
    },
    teen: {
      name: "Teen",
      email: 'teen@teen.com',
      password: 'password',
      password_confirmation: 'password',
      phone: '88000900400',
      date_of_birth: "10/10/1992",
      time_zone: "Hawaii"
    },
    coach: {
      name: "Coach",
      email: 'coach@coach.com',
      password: 'password',
      password_confirmation: 'password'
    },
    admin_user: {
      email: "admin@example.com",
      password: "password"
    }
  }
end

def create_data
  @data = {
    guideline:{
     title: "Test Guideline",
     content: "Content for Test Guideline ..."
    },
    daily_privilege:{
     name: "Test Daily Privilege",
    },
    page:{
     body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu orci a turpis rhoncus lobortis. Aliquam erat volutpat.",
     meta_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
     meta_keywords: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
     meta_title: "Lorem ipsum dolor.",
     visible: true,
     slug: "/lorem/ipsum",
     title: "lorem"
    },
    incident_report: {
      specifics: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu orci a turpis rhoncus lobortis. Aliquam erat volutpat.",
      teen_correction: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu orci a turpis rhoncus lobortis.",
      teen_appeal: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu orci a turpis rhoncus lobortis."
    }
  }
end

def create_data_model(klass)
  data_class = klass.to_s.classify.constantize
  @data_model ||= data_class.create(@data[klass])
end

def find_user(klass = :family)
  user_class = klass.to_s.classify.constantize
  @user ||= user_class.where(email: @visitor[klass][:email]).first
end

def create_user(klass = :family)
  user_class = klass.to_s.classify.constantize
  @visitor[klass][:family_id] = Family.last.try(:id) if user_class == Teen
  @user = user_class.create!(@visitor[klass])
  @user.confirm! rescue nil
  @user
end

def logout
  begin
    visit destroy_user_session_path
  rescue
    puts "User is not logged in!"
  end
end

def fill_form(klass)
  @visitor[klass].each do |key, value|
    if value == true || value == false
      page.find("##{klass}_#{key}").set(value)
    else
      fill_in "#{klass}_#{key}", with: value
    end
  end
end

def fill_data_form(klass)
  @data[klass].each do |key, value|
    if value == true || value == false
      page.find("##{klass}_#{key}").set(value)
    else
      fill_in "#{klass}_#{key}", with: value rescue nil
    end
  end
end

def create_teen
  visit_teens_section
  page.find("#add_teen").click
  fill_form(:teen)
  click_button "Add Teen"
end

def delete_user(klass = :family)
  user_class = klass.to_s.classify.constantize
  @user ||= user_class.where(email: @visitor[:email]).first
  @user.destroy unless @user.nil?
end

def sign_up
  delete_user
  visit new_family_registration_path
  @visitor[:family].delete(:time_zone)
  @visitor[:family].each do |key, value|
    fill_in "family_#{key}", with: value
  end
  click_button "Sign up"
  find_user
end

def sign_in
  log_in
end

def log_in(klass = :family)
  click_link "Login"
  fill_in "user_email", with: @visitor[klass][:email]
  fill_in "user_password", with: @visitor[klass][:password]
  click_button "Sign in"
end

def visit_teens_section
  click_link "Control Panel"
  click_link 'Teens'
end
