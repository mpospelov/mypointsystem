Given /^I have one teen$/ do
  @user.teens.create(@visitor[:teen])
end
Given /^I have one type of inappropriate behavior$/ do
  TypeOfInappropriateBehavior.create(behavior: "Test behavior")
end

When /^I create incident report with a valid data$/ do
  create_data
  visit welcome_path
  click_link "Teen"
  click_link "Incident Reports"
  click_link "Add Incident Report"
  fill_data_form(:incident_report)
  click_button "Create Incident report"
end

Then /^I should see incident report in the list of reports$/ do
  assert page.has_content?("Test behavior"), "Fail"
end

Then /^I should see penalty on next day$/ do
  time_travel_to = @user.time.end_of_day.utc + 1.minutes
  Timecop.travel(time_travel_to)
  Capybara.reset_sessions!
  visit root_path
  log_in(:teen)
  Teen.beginning_of_day
  click_link "Daily Privileges"
  assert page.has_content?("44"), "Fail"
end
