Given /^I am not logged in$/ do
  visit destroy_user_session_path
end

Given /^I am logged in as a "(.*?)"$/ do |user|
  logout
  create_visitor
  #@visitor[:user].delete(:time_zone)
  create_user(user.to_sym) rescue nil
  visit '/'
  log_in(user.to_sym)
  page.find(".alertify-log").click
end

Given /^I am logged in$/ do
  create_user
  sign_in
end

Given /^I exist as a user$/ do
  create_user
end

Given /^I do not exist as a user$/ do
  create_visitor
  delete_user
end

Given /^I exist as an unconfirmed user$/ do
  create_unconfirmed_user
end

When /^I sign in with valid credentials$/ do
  create_visitor
  sign_in
end

When /^I sign out$/ do
  visit '/users/sign_out'
end

Given /^I am login as an admin$/ do
  create_visitor
  create_user(:admin_user)
  visit '/admin'
  fill_form(:admin_user)
  click_button "Login"
  assert page.has_content?("Signed in successfully."), "Fail"
end

