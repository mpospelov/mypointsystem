When /^I create self correct for incident report$/ do
  click_link "Incidents"
  page.find("table a:first").click
  click_link "Self Correct"
  fill_data_form(:incident_report)
  click_button "Self Correct"
end

Then /^I should get half of penalty on next day$/ do
  time_travel_to = @user.time.end_of_day.utc + 1.minutes
  Timecop.travel(time_travel_to)
  Capybara.reset_sessions!
  visit root_path
  log_in(:teen)
  Teen.beginning_of_day
  click_link "Daily Privileges"
  assert page.has_content?("47"), "Fail"
end

When /^I review self correct for incident report$/ do
  click_link "Teen"
  click_link "Incident Reports"
  click_link "History"
  page.find("a.review").click
  page.find("label.radio:nth-child(2)").click
  click_button "Self Correct"
end
