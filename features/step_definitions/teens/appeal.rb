When /^I create appeal for incident report$/ do
  click_link "Incidents"
  page.find("table a:first").click
  click_link "Appeal"
  fill_data_form(:incident_report)
  click_button "Appeal"
end

When /^I review appeal for incident report$/ do
  click_link "Teen"
  click_link "Incident Reports"
  click_link "History"
  page.find("a.review").click
  page.find("label.radio:nth-child(1)").click
  click_button "Revert"
end

Then /^I should prevent penalty on next day$/ do
  time_travel_to = @user.time.end_of_day.utc + 1.minutes
  Timecop.travel(time_travel_to)
  Capybara.reset_sessions!
  visit root_path
  log_in(:teen)
  Teen.beginning_of_day
  click_link "Daily Privileges"
  assert page.has_content?("50"), "Fail"
end
