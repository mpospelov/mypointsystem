Given /^I am logged in as a family$/ do
  create_visitor
  @visitor[:teen].delete(:time_zone)
  create_user
  visit '/'
  log_in
  page.find(".alertify-log").click
  click_link "Welcome #{@user.welcome}"
end

When /^I create Teen in Teen's section with valid data$/ do
  create_teen
  assert @user.teens.present?, "No Teens"
end

Then /^I have a new list item in list of teens$/ do
  assert page.has_content?(@visitor[:teen][:name]), "No Teens"
end

When /^I create Teen in Teen's section with invalid data$/ do
  @visitor[:teen][:email] = ""
  create_teen
end

Then /^I should see an error messages$/ do
  assert page.has_content?("can't be blank"), "No error message"
end

When /^I update Teen in Teen's section$/ do
  create_teen
  click_link @visitor[:teen][:name]
  fill_form :teen
  click_button "Update Teen"
end

Then /^I should see successfull updating message$/ do
  assert page.has_content?("Teen was successfully updated."), "No success message"
end

When /^I create Daily Privilege$/ do
  teen = create_user(:teen)
  create_data
  visit settings_family_teen_path(teen)
  click_link "Daily Privileges"
  page.find("#new_daily_privilege").click
  fill_data_form(:daily_privilege)
  click_button "Create Daily privilege"
end

Then /^I should see active Daily Privilege in teen's list of Daily Privileges$/ do
  assert page.has_css?("li[data-active='1']"), "No New Item In list"
  assert page.has_content?(@data[:daily_privilege][:name]), "Incorrect name of Daily Privilege"
end

When /^I create default Daily Privilege$/ do
  teen = create_user(:teen)
  visit settings_family_teen_path(teen)
  DefaultDailyPrivilege.create(name: "Default Daily Privilege")
end

Then /^I should see inactive Daily Privilege in teen's list of Daily Privileges$/ do
  click_link "Daily Privileges"
  assert page.has_css?("li[data-active='0']"), "No New Item In list"
  assert page.has_content?("Default Daily Privilege"), "Incorrect name of Daily Privilege"
end

When /^I create Guidelines$/ do
  teen = create_user(:teen)
  create_data
  visit settings_family_teen_path(teen)
  click_link "Guidelines"
  page.find("#new_guideline_button").click
  fill_data_form(:guideline)
  click_button "Create Guideline"
end

Then /^I should see active Guideline in teen's list of Guidelines$/ do
  assert page.has_css?("li[data-active='1']"), "No New Item In list"
  assert page.has_content?(@data[:guideline][:title]), "Incorrect name of Guideline"
end

When /^I create default Guideline$/ do
  teen = create_user(:teen)
  visit settings_family_teen_path(teen)
  DefaultGuideline.create(title: "Default Guideline")
end

Then /^I should see inactive Guideline in teen's list of Guideline$/ do
  click_link "Guidelines"
  assert page.has_css?("li[data-active='0']"), "No New Item In list"
  assert page.has_content?("Default Guideline"), "Incorrect name of Guideline"
end
