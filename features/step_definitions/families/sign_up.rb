### WHEN ###
When /^I sign up with valid family data$/ do
  create_visitor
  sign_up
end

When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor[:family] = @visitor[:family].merge(email: "notanemail")
  sign_up
end

When /^I sign up without a password$/ do
  create_visitor
  @visitor[:family] = @visitor[:family].merge(password: "")
  sign_up
end

When /^I sign up with a mismatched password confirmation$/ do
  create_visitor
  @visitor[:family] = @visitor[:family].merge(password_confirmation: "changeme123")
  sign_up
end

When /^I return to the site$/ do
  visit '/'
end

When /^I sign in with a wrong email$/ do
  @visitor[:family] = @visitor[:family].merge(email: "wrong@example.com")
  sign_in
end

When /^I sign in with a wrong password$/ do
  @visitor[:family] = @visitor[:family].merge(password: "wrongpass")
  sign_in
end

### THEN ###

Then /^I redirected to root path$/ do
  assert current_path == '/', "Wrong page"
end

Then /^I should see an invalid email message$/ do
  assert page.has_content? "Email is invalid"
end

Then /^I should see a missing password message$/ do
  assert page.has_content? "Password can't be blank"
end

Then /^I should see a mismatched password message$/ do
  assert page.has_content? "Password doesn't match confirmation"
end

Then /^I should see a signed out message$/ do
  assert page.has_content? "Signed out successfully."
end

Then /^I see an invalid login message$/ do
  assert page.has_content? "Invalid email or password."
end

Then /^I should see an account edited message$/ do
  assert page.has_content? "You updated your account successfully."
end

Then /^I should see my name$/ do
  create_user
  assert page.has_content? @user[:name]
end
