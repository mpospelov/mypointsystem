When /^I create "(.*?)" in admin panel with valid data$/ do |klass|
  klass_sym = klass.downcase.to_sym
  create_data
  visit eval("admin_#{klass_sym.to_s.pluralize}_path")
  click_link "New #{klass}"
  fill_data_form(klass_sym)
  click_button "Create #{klass}"
end

Then /^I can visit this page by it's slug/ do
  visit root_path
  click_link @data[:page][:title]
  assert page.has_content?(@data[:page][:body]), "Wrong content"
end

When /^I create "Page" in admin panel without "(.*?)"$/ do |without|
  visit admin_pages_path
  click_link "New Page"
  create_data
  @data[:page][without] = ""
  fill_data_form(:page)
  click_button "Create Page"
end

Then /^I should see an error message "(.*?)"$/ do |message|
  assert page.has_css?('p.inline-errors', text: message), "Not correct error message"
end

When /^I create "(.*?)" in admin panel with invalid slug$/ do |arg1|
  visit admin_pages_path
  click_link "New Page"
  create_data
  @data[:page][:slug] = 'lorem'
  fill_data_form(:page)
  click_button "Create Page"
end
