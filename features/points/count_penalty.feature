@javascript
Feature: Count penalty
  In order to count penalty
  As a family
  I want to be able to create incident reports and see the teens penalty

    Background:
      Given I am logged in as a family
      Given I have one teen
      Given I have one type of inappropriate behavior

    Scenario: Family create incident report with a valid data
      When I create incident report with a valid data
      Then I should see incident report in the list of reports
      Then I should see penalty on next day
