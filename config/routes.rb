require 'sidekiq/web'
TestMyPointSystem::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  mount Private::API => '/api/private'
  mount Sidekiq::Web => '/sidekiq'

  namespace :family do
    resources :teens do
      resources :incident_reports do
        get 'history', :on => :collection
        member do
          get "review_self_correct"
          get "review_appeal"
        end
      end
      resources :guidelines
      resources :daily_privileges do
        collection do
          post 'sort' => "daily_privileges#sort"
        end
      end
      member do
        get 'settings' => "teens#settings"
      end
    end
    resources :subscription_plans
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :users, controllers: { :omniauth_callbacks => "users/omniauth_callbacks" }
  devise_for :families, controllers: { confirmations: 'confirmations', :omniauth_callbacks => "users/omniauth_callbacks" }

  namespace :teen do
    resources :incident_reports do
      member do
        get 'self_correct'
        get 'appeal'
      end
    end
    resources :guidelines
    resources :daily_privileges
  end

  #devise_scope :family do
  #  get '/families/auth/:provider' => 'users/omniauth_callbacks#passthru'
  #end
  #devise_scope :user do
  #  get '/users/auth/:provider' => 'users/omniauth_callbacks#passthru'
  #end

  resources :users
  get '/account_settings' => "users#account_settings"
  get '/controll_panel' => 'users#controll_panel'
  get '/welcome' => 'users#tiles'

  root :to => 'home_page#index'

  ActiveAdmin.routes(self)
  get "/*slug" => "pages#show"
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
