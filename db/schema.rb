# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130608121133) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "daily_privileges", :force => true do |t|
    t.string   "name"
    t.string   "type"
    t.integer  "family_id"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "teen_id"
    t.boolean  "active",     :default => true
    t.boolean  "default",    :default => false
    t.integer  "position"
  end

  create_table "daily_privileges_teens", :id => false, :force => true do |t|
    t.integer "daily_privilege_id"
    t.integer "teen_id"
  end

  add_index "daily_privileges_teens", ["daily_privilege_id", "teen_id"], :name => "index_daily_privileges_teens_on_daily_privilege_id_and_teen_id"
  add_index "daily_privileges_teens", ["teen_id", "daily_privilege_id"], :name => "index_daily_privileges_teens_on_teen_id_and_daily_privilege_id"

  create_table "guidelines", :force => true do |t|
    t.string   "title"
    t.text     "recommendation"
    t.text     "content"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "user_id"
    t.boolean  "active",         :default => true
    t.string   "type"
    t.integer  "coach_id"
    t.integer  "teen_id"
    t.boolean  "default",        :default => false
  end

  create_table "guidelines_teens", :id => false, :force => true do |t|
    t.integer "guideline_id"
    t.integer "teen_id"
  end

  add_index "guidelines_teens", ["guideline_id", "teen_id"], :name => "index_guidelines_teens_on_guideline_id_and_teen_id"
  add_index "guidelines_teens", ["teen_id", "guideline_id"], :name => "index_guidelines_teens_on_teen_id_and_guideline_id"

  create_table "incident_reports", :force => true do |t|
    t.integer  "teen_id"
    t.string   "type_of_inappropriate_behavior"
    t.text     "specifics"
    t.string   "severity"
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.integer  "family_id"
    t.text     "teen_correction"
    t.text     "teen_appeal"
    t.integer  "points_loss"
    t.boolean  "reviewed",                       :default => false
    t.boolean  "appealed",                       :default => false
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "slug"
    t.string   "meta_description"
    t.string   "meta_title"
    t.string   "meta_keywords"
    t.integer  "position"
    t.boolean  "visible"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.boolean  "is_main"
  end

  create_table "type_of_inappropriate_behaviors", :force => true do |t|
    t.string   "behavior"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "uploaded_files", :force => true do |t|
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "file_carrier_id"
    t.string   "file_carrier_type"
    t.string   "type"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updated_at"
    t.string   "description"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "father_name"
    t.string   "mother_name"
    t.string   "phone"
    t.string   "key"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "time_zone"
    t.string   "provider"
    t.string   "uid"
    t.string   "type"
    t.integer  "user_id"
    t.string   "name"
    t.string   "date_of_birth"
    t.integer  "points",                 :default => 50
  end

  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
