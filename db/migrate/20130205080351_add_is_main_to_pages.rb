class AddIsMainToPages < ActiveRecord::Migration
  def change
    add_column :pages, :is_main, :boolean
  end
end
