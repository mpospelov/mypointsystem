class AddTeenIdToDailyPrivileges < ActiveRecord::Migration
  def change
    add_column :daily_privileges, :teen_id, :integer
  end
end
