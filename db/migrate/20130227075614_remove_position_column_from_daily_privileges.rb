class RemovePositionColumnFromDailyPrivileges < ActiveRecord::Migration
  def up
    remove_column :daily_privileges, :position
  end

  def down
  end
end
