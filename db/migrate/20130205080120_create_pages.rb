class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :body
      t.string :slug
      t.string :meta_description
      t.string :meta_title
      t.string :meta_keywords
      t.integer :position
      t.boolean :visible

      t.timestamps
    end
  end
end
