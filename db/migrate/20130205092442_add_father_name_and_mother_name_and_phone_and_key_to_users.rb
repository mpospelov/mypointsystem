class AddFatherNameAndMotherNameAndPhoneAndKeyToUsers < ActiveRecord::Migration
  def change
    add_column :users, :father_name, :string
    add_column :users, :mother_name, :string
    add_column :users, :phone, :string
    add_column :users, :key, :string
  end
end
