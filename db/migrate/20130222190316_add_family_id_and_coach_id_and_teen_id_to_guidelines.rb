class AddFamilyIdAndCoachIdAndTeenIdToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :coach_id, :integer
    add_column :guidelines, :teen_id, :integer
  end
end
