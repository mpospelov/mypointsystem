class AddPositionToDailyPrivileges < ActiveRecord::Migration
  def change
    add_column :daily_privileges, :position, :integer
  end
end
