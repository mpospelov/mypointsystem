class DeleteAllGuidelinesAndDailyPrivileges0 < ActiveRecord::Migration
  def up
    Guideline.destroy_all
    DailyPrivilege.destroy_all
    User.destroy_all
  end

  def down
  end
end
