class DestroyUselessFields < ActiveRecord::Migration
  def up
    drop_attached_file :users, :avatar
  end

  def down
  end
end
