class CreateTeens < ActiveRecord::Migration
  def change
    create_table :teens do |t|
      t.string :name
      t.date :date_of_birth
      t.string :email
      t.string :password

      t.timestamps
    end
  end
end
