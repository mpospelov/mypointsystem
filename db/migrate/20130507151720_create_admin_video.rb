class CreateAdminVideo < ActiveRecord::Migration
  def up
    Video.create if Video.count == 0
  end

  def down
    Video.destroy_all
  end
end
