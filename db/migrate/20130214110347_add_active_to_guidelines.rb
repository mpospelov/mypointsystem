class AddActiveToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :active, :boolean, :default => true
  end
end
