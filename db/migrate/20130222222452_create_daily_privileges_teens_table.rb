class CreateDailyPrivilegesTeensTable < ActiveRecord::Migration
  def self.up
    create_table :daily_privileges_teens, :id => false do |t|
      t.references :daily_privilege
      t.references :teen
    end
    add_index :daily_privileges_teens, [:daily_privilege_id, :teen_id]
    add_index :daily_privileges_teens, [:teen_id, :daily_privilege_id]
  end

  def self.down
    drop_table :daily_privileges_teens
  end

end
