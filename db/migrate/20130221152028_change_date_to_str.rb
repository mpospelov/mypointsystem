class ChangeDateToStr < ActiveRecord::Migration
  def up
    drop_table :teens
    add_column :users, :name, :string
    add_column :users, :date_of_birth, :string
    change_table :users do |t|
      t.attachment :avatar
    end
  end
  def down
  end
end
