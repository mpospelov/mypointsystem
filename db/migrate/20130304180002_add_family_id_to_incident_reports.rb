class AddFamilyIdToIncidentReports < ActiveRecord::Migration
  def change
    add_column :incident_reports, :family_id, :integer
  end
end
