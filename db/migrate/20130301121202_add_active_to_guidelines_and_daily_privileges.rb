class AddActiveToGuidelinesAndDailyPrivileges < ActiveRecord::Migration
  def change
    #add_column :guidelines, :active, :boolean, :default => true
    add_column :daily_privileges, :active, :boolean, :default => true
  end
end
