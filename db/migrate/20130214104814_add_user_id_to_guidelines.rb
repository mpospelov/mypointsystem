class AddUserIdToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :user_id, :integer
  end
end
