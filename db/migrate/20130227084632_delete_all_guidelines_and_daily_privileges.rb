class DeleteAllGuidelinesAndDailyPrivileges < ActiveRecord::Migration
  def up
    Guideline.destroy_all
    DailyPrivilege.destroy_all
  end

  def down
  end
end
