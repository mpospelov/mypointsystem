class ChangeUsersTimeZoneToDefault < ActiveRecord::Migration
  def up
    User.update_all(:time_zone => "Central Time (US & Canada)")
  end

  def down
  end
end
