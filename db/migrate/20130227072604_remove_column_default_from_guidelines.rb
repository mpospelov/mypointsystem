class RemoveColumnDefaultFromGuidelines < ActiveRecord::Migration
  def up
    remove_column :guidelines, :default
  end

  def down
  end
end
