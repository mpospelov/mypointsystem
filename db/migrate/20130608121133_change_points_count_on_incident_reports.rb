class ChangePointsCountOnIncidentReports < ActiveRecord::Migration
  def up
    add_column :incident_reports, :appealed, :boolean, :default => false
    rename_column :incident_reports, :review_by_parent, :points_loss
  end

  def down
    remove_column :incident_reports, :appealed
  end
end
