class AddTeenCorrectionToIncidentReports < ActiveRecord::Migration
  def change
    add_column :incident_reports, :teen_correction, :text
  end
end
