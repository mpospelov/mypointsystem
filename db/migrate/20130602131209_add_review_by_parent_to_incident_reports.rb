class AddReviewByParentToIncidentReports < ActiveRecord::Migration
  def change
    add_column :incident_reports, :review_by_parent, :integer
  end
end
