class RenamePasswordTeen < ActiveRecord::Migration
  def up
    rename_column(:teens, :password, :encrypted_password)
  end

  def down
  end
end
