class AddPhoneToTeens < ActiveRecord::Migration
  def change
    add_column :teens, :phone, :string
  end
end
