class AddAttachmentDataToUploadedFile < ActiveRecord::Migration
  def self.up
    add_column :uploaded_files, :data_file_name, :string
    add_column :uploaded_files, :data_content_type, :string
    add_column :uploaded_files, :data_file_size, :integer
    add_column :uploaded_files, :data_updated_at, :datetime
  end

  def self.down
    remove_column :uploaded_files, :data_file_name
    remove_column :uploaded_files, :data_content_type
    remove_column :uploaded_files, :data_file_size
    remove_column :uploaded_files, :data_updated_at
  end
end
