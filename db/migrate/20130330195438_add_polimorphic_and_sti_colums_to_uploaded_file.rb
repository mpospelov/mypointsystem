class AddPolimorphicAndStiColumsToUploadedFile < ActiveRecord::Migration
  def change
    change_table :uploaded_files do |t|
      t.references :file_carrier, :polymorphic => true
      t.string :type
    end
  end
end
