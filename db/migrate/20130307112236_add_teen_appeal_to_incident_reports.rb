class AddTeenAppealToIncidentReports < ActiveRecord::Migration
  def change
    add_column :incident_reports, :teen_appeal, :text
  end
end
