class AddAttachmentAvatarToTeens < ActiveRecord::Migration
  def self.up
    change_table :teens do |t|
      t.attachment :avatar
    end
  end

  def self.down
    drop_attached_file :teens, :avatar
  end
end
