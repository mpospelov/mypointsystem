class CreateTypeOfInappropriateBehaviors < ActiveRecord::Migration
  def change
    create_table :type_of_inappropriate_behaviors do |t|
      t.string :behavior

      t.timestamps
    end
  end
end
