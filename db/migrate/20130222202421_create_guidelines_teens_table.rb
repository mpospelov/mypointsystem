class CreateGuidelinesTeensTable < ActiveRecord::Migration
  def self.up
    create_table :guidelines_teens, :id => false do |t|
      t.references :guideline
      t.references :teen
    end
    add_index :guidelines_teens, [:guideline_id, :teen_id]
    add_index :guidelines_teens, [:teen_id, :guideline_id]
  end

  def self.down
    drop_table :guidelines_teens
  end
end
