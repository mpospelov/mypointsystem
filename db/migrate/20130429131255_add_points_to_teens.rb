class AddPointsToTeens < ActiveRecord::Migration
  def change
    add_column :users, :points, :integer, :default => 50
  end
end
