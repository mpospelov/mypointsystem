class CreateIncidentReports < ActiveRecord::Migration
  def change
    create_table :incident_reports do |t|
      t.integer :teen_id
      t.string :type_of_inappropriate_behavior
      t.text :specifics
      t.string :severity

      t.timestamps
    end
  end
end
