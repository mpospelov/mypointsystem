class CreateDailyPrivileges < ActiveRecord::Migration
  def change
    create_table :daily_privileges do |t|
      t.string :name
      t.string :type
      t.integer :family_id

      t.timestamps
    end
  end
end
