class AddDefaultToGuidelinesAndDailyPrivileges < ActiveRecord::Migration
  def change
    add_column :guidelines, :default, :boolean, :default => false
    add_column :daily_privileges, :default, :boolean, :default => false
  end
end
