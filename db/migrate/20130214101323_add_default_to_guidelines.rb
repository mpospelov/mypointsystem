class AddDefaultToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :default,:boolean, :default => false
  end
end
