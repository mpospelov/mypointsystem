class AddReviwedToIncidentReports < ActiveRecord::Migration
  def change
    add_column :incident_reports, :reviewed, :boolean, :default => false
  end
end
