class AddTypeToGuidelines < ActiveRecord::Migration
  def change
    add_column :guidelines, :type, :string
  end
end
